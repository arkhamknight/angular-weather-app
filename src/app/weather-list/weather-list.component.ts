import { Component, OnInit } from '@angular/core';
import {WeatherService} from "../weather.service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {
  weathers = []
  constructor(
    private weatherService: WeatherService,
    private router: Router
  ) { }

  ngOnInit() {
    this.weatherService.getWeather().subscribe(response => {
      this.weathers = response.list     
      console.log(this.weathers);      
    })
  }

  onWeatherClicked(weather) {
    this.weatherService.setCurrentWeather(weather)
    this.router.navigateByUrl('/detail')
  }
}
