import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {

  weather = {}

  constructor(
    private service: WeatherService
  ) { }

  ngOnInit() {
    this.weather = this.service.getCurrentWeather()
  }

}
